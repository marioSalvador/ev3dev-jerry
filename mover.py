#!/usr/bin/env python3
import time
import termios
import tty
import ev3dev.ev3 as ev3
import sys

motor_left = ev3.LargeMotor('outB')
motor_right = ev3.LargeMotor('outC')
motor_a = ev3.MediumMotor('outA')

#==============================================

def getch():
   fd = sys.stdin.fileno()
   old_settings = termios.tcgetattr(fd)
   try:
      tty.setraw(fd)
      ch = sys.stdin.read(1)
   finally:
      termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
   
   return ch

#==============================================

def fire():
   motor_a.run_direct(duty_cycle_sp=20)

#==============================================

def fire2():
   motor_a.run_direct(duty_cycle_sp=-20)
   motor_a.stop(stop_action='brake')
#=========
def fireStop():
   motor_a.stop(stop_action='brake')
#=========
def forward():
   motor_left.run_direct(duty_cycle_sp=100)
   motor_right.run_direct(duty_cycle_sp=100)

#==============================================

def back():
   motor_left.run_direct(duty_cycle_sp=-100)
   motor_right.run_direct(duty_cycle_sp=-100)

#==============================================

def left():
   motor_left.run_direct( duty_cycle_sp=-75)
   motor_right.run_direct( duty_cycle_sp=75)

#==============================================

def right():
   motor_left.run_direct( duty_cycle_sp=75)
   motor_right.run_direct( duty_cycle_sp=-75)

#==============================================

def leftMov():
   motor_left.run_direct( duty_cycle_sp= 30)
   motor_right.run_direct( duty_cycle_sp=100)

#==============================================

def rightMov():
   motor_left.run_direct( duty_cycle_sp=100)
   motor_right.run_direct( duty_cycle_sp=30)



def stop():
   motor_left.run_direct( duty_cycle_sp=0)
   motor_right.run_direct( duty_cycle_sp=-0)



def bep():
   ev3.Sound.tone(500,1000).wait()
#==============================================

while True:
   k = getch()
   print(k)
   if k == 'w':
      forward()
   if k == 's':
      back()
   if k == 'a':
      left()
   if k == 'd':
      right()
   if k == 'f':
      fire()
   if k == 'g':
      fire2()
   if k == 'v':
      fireStop()
   
   if k == 'b':
      bep()

   if k == 'q':
      leftMov()

   if k == 'e':
      rightMov()



   if k == ' ':
      stop()


   if k == 'p':
      exit()
