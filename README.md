**Robot recoje basura**

Proyecto para la clase de laboratorio temático.

creado por:
	Pamela Tovar Scorcia
	Uriel Ruiz Santiago
	Antonio Hernándes Moreno
	Mario Salvador Torres
**

---

## Instrucciones

Instalar git en sus computadoras:

1. instalar git en sus computadoras
	sudo apt-get install git
2. Ubicarse en la carpeta donde deseen descargar el repositorio
3. Clonar repositorio de forma local
	git clone https://marioSalvador@bitbucket.org/marioSalvador/ev3dev-jerry.git
4. Ahora pueden editar el codigo y guardarlo
5. Para subir los cambios realizados al repositorio remoto:
	5.1 Agregar los archivos que se han modificado
		git add [archivo]   (archivo por archivo)
		git add .		(todos los archivos)
	5.2 Crear commit para subirlo al repositorio
		git commit -m "Agregan la descripción de la modificación que hicieron"
	5.3 Subir todos los cambios al repositorio
		git push origin master

6. Para actualizar el repositorio local con la nueva versión del repositorio remoto.
	git pull origin master

---

## Pequeño tutorial de git

http://rogerdudler.github.io/git-guide/index.es.html