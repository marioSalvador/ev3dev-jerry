#!/usr/bin/env python3
import ev3dev.ev3 as ev3
from threading import Thread
from time import sleep

#Registro: lista de hilos
#threads = list()

#Instancia de los motores y sensores a utilizar
motor_left = ev3.LargeMotor('outB')
motor_right = ev3.LargeMotor('outC')
sens_color = ev3.ColorSensor()
infrarojo = ev3.InfraredSensor()
touch_sensor = ev3.TouchSensor()

assert sens_color	
assert touch_sensor

#modo para lectura de colores 2 = BLUE
sens_color.mode	= 'COL-COLOR'

#Banderas para inicar si determinado sensor está activo = 0 o inactivo = 1
f_color = 0  
f_infraRed = 0


#funcion para avanzar hacia adelante
def avanzar(): 
	motor_right.run_forever(speed_sp = 1000)
	motor_left.run_forever( speed_sp = 1000)

def parar():
	motor_right.stop(stop_action='brake')
	motor_left.stop(stop_action='brake')

def gira_izquierda():
	motor_left.run_direct(duty_cycle_sp=-80)
	motor_right.run_direct(duty_cycle_sp=80)
	sleep(1.5)
	parar()

def gira_derecha():
	motor_left.run_direct(duty_cycle_sp=80)
	motor_right.run_direct(duty_cycle_sp=-80)
	sleep(1.5)
	parar()

def basura_encontrada():
	parar()
	print("basura encontrada")
	ev3.Sound.speak('Basura encontrada').wait()

def contenedor_encontrado():
	parar()
	ev3.Sound.speak('Contenedor encontrada').wait()


#función objetivo para el hilo 1
def detecta_obstaculo():
	print("Hilo detecta_obstaculo")
	f_infraRed = 1
	while f_infraRed == 1:
		if infrarojo.value() < 45:																					
			parar()
			f_infraRed = 0
	return

#funcion objetivo para el hilo 2
def detecta_arena():
	print("Hilo detecta_arena")
	while not touch_sensor.is_pressed:
		avanzar()
		print (sens_color.value())
		if (sens_color.value() == ev3.ColorSensor.COLOR_BLUE):  #si encuentra agua
			gira_izquierda()
		elif (sens_color.value() == ev3.ColorSensor.COLOR_BLACK):  #si encuentra basura
			basura_encontrada()
		elif (sens_color.value() == ev3.ColorSensor.COLOR_RED):
			contenedor_encontrado()

	motor_left.stop()
	motor_right.stop()
	print("Se paró hilo_1")
	exit()

def main():

	hColor = Thread(target=detecta_arena)
	hColor.start()

	#hInfra = Thread(target=detecta_obstaculo)
	#hInfra.start()
	#matriz = []
	#for i in range(20):
		#matriz.append([])
	    #for j in range(20):
	    	#matriz[i].append(None)
	exit()
main()
