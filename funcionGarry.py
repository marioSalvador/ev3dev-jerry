#!/usr/bin/env python3 
import ev3dev.ev3 as ev3 #se importa la libreria de ev3 para controlar sus motores o sensores del robot
from time import sleep #se importa la función sleep de la libreria time
import threading
motor_left = ev3.LargeMotor('outB') #Se asigna el motro izquierdo del robot a "motor_left", se indica que está conectado en el puerto B del Brick
motor_right = ev3.LargeMotor('outC')#Se asigna el motro derecho del robot a "motor_rigth", se indica que está conectado en el puerto C del Brick
motor_a = ev3.MediumMotor('outA') #Se asigna el motro medium del robot a "motor_a", se indica que está conectado en el puerto A del Brick
infrarojo = ev3.InfraredSensor() ##Se asigna el sensor de proximidad a "infrarrojo", se asigna por default al puerto 1

#Función que sirve para que el robot avance
def avanzar(): 
	# el motor derecho e izquierdo avanzaran mientras que no se les indique el paro, 
	#"speed_sp" es para indicar la velocidad del motor, se puede poner de 100 a 1000 la velocidad.
	motor_right.run_forever(speed_sp=1000)
	motor_left.run_forever(speed_sp=1000)



#Esta función detiene los motores del robot
def parar():
	motor_right.stop(stop_action='brake') #Se le indica al motor derecho que se detenga.
	motor_left.stop(stop_action='brake')#Se le indica al motor izquierdo que se detenga.



#Esta funcio hace que el robot gire hacia la izquierda sobre su propio eje
def gira_izquierda():
	#Para que gire el robot, se le indica que el motor derecho avance y el motor izquierdo retroceda.
	motor_left.run_direct(duty_cycle_sp=-100)  #"duty_cycle_sp" es para que el motor gire y los valores que se le pueden dar es de 0 a 100
	motor_right.run_direct(duty_cycle_sp=100)  #"duty_cycle_sp" es para que el motor gire y los valores que se le pueden dar es de 0 a 100
	sleep(0.8) # sleep es una funcion que hace que el robot gire solo por 0.8 segungos.
	parar()




#Esta funcio hace que el robot gire hacia la izquierda sobre su propio eje
def gira_derecha():
	#Para que gire el robot, se le indica que el motor izquierdo avance y el motor derecho retroceda.
	motor_left.run_direct(duty_cycle_sp=100)   #"duty_cycle_sp" es para que el motor gire y los valores que se le pueden dar es de 0 a 100
	motor_right.run_direct(duty_cycle_sp=-100) #"duty_cycle_sp" es para que el motor gire y los valores que se le pueden dar es de 0 a 100
	sleep(0.8) # sleep es una funcion que hace que el robot gire solo por 0.8 segungos.
	parar()



#Esta funcion hace que abra y cierre la garra el robot, al inicio, el robot tiene la garra abirta.
#la velocidad debe estar en 60 y el sleep en 0.6'para que abra de forma correcta
def moverGarra():
	motor_a.run_direct(duty_cycle_sp=-60) #El Medium Motor gira a la izquierda con una velocidad de 60
	sleep(0.6) #El motor girará durante 0.6 segundos, esto es suficiente para que la garra cierre.
	motor_a.stop(stop_action='brake') # se detiene el motor
	sleep(0.6) #Espera 0.6 segundos el robot para que abra la garra.
	motor_a.run_direct(duty_cycle_sp=60) #El Medium Motor gira a la derecha con una velocidad de 60
	sleep(0.6) #El motor girará durante 0.6 segundos, esto es suficiente para que la garra vuelva a abrir.
	motor_a.stop(stop_action='brake') #Se detiene la garra y vuelve a quedar abierta.






#La funcion garry, es la función principal y desde aquí se utilizan las demás funciones dependiendo de las decisiones que tome el robot.
def garry():
	contador = 0 #Se inicializa la variable contador en 0, esta variable será la que contará el número de basuras que detecte el robot.

	colorDetectado = ev3.ColorSensor() #Se define colorDetectado al sensor de color.
	touch_sensor = ev3.TouchSensor() #Se define touch_sensor al sensor touch.

	while not touch_sensor.is_pressed: # el robot se movera y hará todas las funciones que se indiquen mientras que no sea oprimido el sensor touch.
		cont = 0 # Se declara e inicializa la variable en 0, esta variable ayuda 
		
		pregunta = 0 #Se utilizará la variable para confirmar el color que detecta el robot,
		#el robot depues de detectar un color, avanzará un poco y confirmara si sigue siendo 
		#el color que había detectado, en caso de que si, la variable seguira en 0, 
		#en caso de que sea otro color, la variable cambiara a 1 verificara que color es.
		
		colorDetectado.mode = 'RGB-RAW' # la variable colorDetectado utilizará el modo de RGB para detectar los colores. el valor que devuelve es de 0 a 255
		#tambien se puede usar otros modos, como "REF-RAW" devuelve (0-100), COL-REFLECT' que devuelve (0-100) y 
		#'COL-COLOR' que tiene color establecidos (0-No color, 1-Black, 2-Blue, 3-Green, 4-Yellow, 5-Red, 6-White, 7-Brown)

		
		#DETECTA OBJETO
		if infrarojo.value() < 35: #si el sensor infrarojo (proximidad) detecta algo a menos de 35cm, lo esquivará.																				
			parar() #Al detectar un objeto, se detendrá.
			print("###OBJETO DETECTADO####") #Imprime en pantalla que detectó un objeto.
			gira_derecha() #Girará a la derecha.
		


		
		#Despues de hacer una calibracion de los colores de la arena, estos son los valores que dieron cada color.
		#Se toma un valor promedio o con mayor frecuencia para poder hacer los rangos
		
		#ROJO 	50, 48, 53
		#CAFE 	27, 23, 29, 20
		#NEGRO  4, 5, 3, 7
		#AZUL   10, 12, 13, 11



		#CAFE
		if colorDetectado.value() > 20 and colorDetectado.value() < 45: #Si el valor del color detectado esta entre 21 y 44.
			avanzar() #el robot avanzará. pero despues confirmará el color.

			#DETECTA OBSTACULO
			if infrarojo.value() < 35:#Vuelve a checar si hay algun obstaculo enfrente																					
				parar()# para
				print("###OBJETO DETECTADO####")#imprime en pantalla que encontro un objeto
				gira_derecha()#gira a la derecha
			
			
			#NEGRO 
			if colorDetectado.value() < 8: # despues de que detectó el cafe, avanza y pregunta si es negro, esto es por si había confundido el cafe con el negro
			#en caso de que realmente sea egro y no cafe, estonces hará las funciones que tiene que hacer con el color negro.
				parar() #para
				pregunta = 1 #la variable pregunta cambia de 0 a 1, esto quiere decir que el robort se había equivocado en la primera decisión
				print("Negro", colorDetectado.value()) #imprime en pantalla que detectó el negro e imprime el valor detectado por sensor
				ev3.Sound.tone(500,1000).wait() #Reproduce un Beep largo el robot, esto hace referencia a que localizó una basura.
				moverGarra() #Mueve la garra, simbolo de que recoge la basura.
				contador += 1 #el contador se incrementa en 1, irá acumulando el numero de basuras.
				gira_derecha() #girará a la derecha para despues seguir avanzando.
				sleep(3) #todo lo anterior lo hará en 3 segundos, sin contar lo que tarda para abrir y cerrar la garra.
			

			#AZUL
			# despues de que detectó el cafe, avanza y pregunta si es azul, esto es por si había confundido el cafe con el azul
			if colorDetectado.value() >= 12 and colorDetectado.value() <=20:# si el nuevo valor detectado es de 12 a 20, entonce...
				pregunta = 1 # la variable pregunta cambia de 0 a 1, esto quiere decir que el robort se había equivocado en la primera decisión
				gira_izquierda() #girará a la izquierda, esto porque no puede meterse o quedarse en el azul que representa el agua.
				print("****************AZUL: ",colorDetectado.value()) #imprime en pantalla que es azul y el valor del color detectado.
			
		

			#ROJO
			if colorDetectado.value() > 49: # despues de que detectó el cafe, avanza y pregunta si es rojo, esto es por si había confundido el cafe con el rojo
				parar() #Se detiene
				pregunta = 1 #la variable pregunta cambia de 0 a 1, esto quiere decir que el robort se había equivocado en la primera decisión

				while contador > 0: #este bucle es para depositar el numero de basuras recolectadas, 
				#mientras el contador de basuras sea mayor a 0, hará las intrucciones que hay dentro
					ev3.Sound.tone(100,400).wait() #Reproduce un beep corto por cada basura que deja.
					
					motor_a.run_direct(duty_cycle_sp=-60)#cierra garra
					sleep(0.5)
					motor_a.stop(stop_action='brake')#para el motor
					
					motor_a.run_direct(duty_cycle_sp=60)#abre garra
					sleep(0.28)
					motor_a.stop(stop_action='brake')#para el motor
					sleep(0.3)
					contador -= 1 #Decrementa el contador en 1, osea que le reta una basura al número total de basuras que tiene.

				contador = 0 #Despues de salir del while, la variable contador la vuelve a dejar en 0 para evitar que se quede con restos la variable
				gira_izquierda() #gira a la izquierda para seguir.

			#COLOR CORRECTO
			if pregunta == 0: # Si la variable es igual a 0, quiere decir que el color detectado es correcto y hará las instrucciones para el colr cafe
				avanzar() #mientras que el color sea cafe, el robot avanzará buscando basuras
			pregunta = 0 #Si el color detectado fuera otro, la variable pregunta llegaria aqui con 1 y como termina aqui la funcion, la vuelve a dejar en 0

		




		#NEGRO 
		if colorDetectado.value() < 8:
			avanzar()#el robot avanzará. pero despues confirmará el color.


			#DETECTA OBSTACULO
			if infrarojo.value() < 35:#Vuelve a checar si hay algun obstaculo enfrente																					
				parar()# para
				print("###OBJETO DETECTADO####")#imprime en pantalla que encontro un objeto
				gira_derecha()#gira a la derecha
			
			#AZUL
			# despues de que detectó el cafe, avanza y pregunta si es azul, esto es por si había confundido el cafe con el azul
			if colorDetectado.value() >= 12 and colorDetectado.value() <=20:# si el nuevo valor detectado es de 12 a 20, entonce...
				pregunta = 1 # la variable pregunta cambia de 0 a 1, esto quiere decir que el robort se había equivocado en la primera decisión
				gira_izquierda() #girará a la izquierda, esto porque no puede meterse o quedarse en el azul que representa el agua.
				print("****************AZUL: ",colorDetectado.value()) #imprime en pantalla que es azul y el valor del color detectado.
			
			

			#CAFE
			if colorDetectado.value() > 20 and colorDetectado.value() < 45: #si el valor detectado es el del cafe, entonces hará las instrucciones del cafe
				pregunta = 1 #la variable cambia de 0 a 1, indicando que el robot cambio de opcion en el color
				avanzar()# sigue avanzando


			#COLOR CORRECTO (NEGRO)
			if pregunta == 0: # Si la variable es igual a 0, quiere decir que el color detectado es correcto y hará las instrucciones para el colr negro
				parar() #para
				print("Negro", colorDetectado.value()) #imprime en pantalla que detectó el negro e imprime el valor detectado por sensor
				ev3.Sound.tone(500,1000).wait() #Reproduce un Beep largo el robot, esto hace referencia a que localizó una basura.
				moverGarra() #Mueve la garra, simbolo de que recoge la basura.
				contador += 1 #el contador se incrementa en 1, irá acumulando el numero de basuras.
				gira_derecha() #girará a la derecha para despues seguir avanzando.
				sleep(3) #todo lo anterior lo hará en 3 segundos, sin contar lo que tarda para abrir y cerrar la garra.
			
			pregunta = 0 #Si el color detectado fuera otro, la variable pregunta llegaria aqui con 1 y como termina aqui la funcion, la vuelve a dejar en 0
		





		#AZUL 
		if colorDetectado.value() >= 8 and colorDetectado.value() <=20:
			avanzar()#el robot avanzará. pero despues confirmará el color.
			
			#DETECTA OBSTACULO
			if infrarojo.value() < 35:#Vuelve a checar si hay algun obstaculo enfrente																					
				parar()# para
				print("###OBJETO DETECTADO####")#imprime en pantalla que encontro un objeto
				gira_derecha()#gira a la derecha
			

			#NEGRO 
			if colorDetectado.value() < 8: # despues de que detectó el cafe, avanza y pregunta si es negro, esto es por si había confundido el cafe con el negro
			#en caso de que realmente sea egro y no cafe, estonces hará las funciones que tiene que hacer con el color negro.
				parar() #para
				pregunta = 1 #la variable pregunta cambia de 0 a 1, esto quiere decir que el robort se había equivocado en la primera decisión
				print("Negro", colorDetectado.value()) #imprime en pantalla que detectó el negro e imprime el valor detectado por sensor
				ev3.Sound.tone(500,1000).wait() #Reproduce un Beep largo el robot, esto hace referencia a que localizó una basura.
				moverGarra() #Mueve la garra, simbolo de que recoge la basura.
				contador += 1 #el contador se incrementa en 1, irá acumulando el numero de basuras.
				gira_derecha() #girará a la derecha para despues seguir avanzando.
				sleep(3) #todo lo anterior lo hará en 3 segundos, sin contar lo que tarda para abrir y cerrar la garra.
			

			#CAFE
			if colorDetectado.value() > 20 and colorDetectado.value() < 45: #si el valor detectado es el del cafe, entonces hará las instrucciones del cafe
				pregunta = 1 #la variable cambia de 0 a 1, indicando que el robot cambio de opcion en el color
				avanzar()# sigue avanzando


			#COLOR CORRECTO (AZUL)
			if pregunta == 0: # Si la variable es igual a 0, quiere decir que el color detectado es correcto y hará las instrucciones para el colr azul
				gira_izquierda() #girará a la izquierda, esto porque no puede meterse o quedarse en el azul que representa el agua.
				print("****************AZUL: ",colorDetectado.value()) #imprime en pantalla que es azul y el valor del color detectado.
			
			pregunta = 0 #Si el color detectado fuera otro, la variable pregunta llegaria aqui con 1 y como termina aqui la funcion, la vuelve a dejar en 0






		#ROJO
		if colorDetectado.value() > 49:
			avanzar()#el robot avanzará. pero despues confirmará el color.
			
			#DETECTA OBSTACULO
			if infrarojo.value() < 35:#Vuelve a checar si hay algun obstaculo enfrente																					
				parar()# para
				print("###OBJETO DETECTADO####")#imprime en pantalla que encontro un objeto
				gira_derecha()#gira a la derecha
			
			
			#CAFE
			if colorDetectado.value() > 20 and colorDetectado.value() < 45: #si el valor detectado es el del cafe, entonces hará las instrucciones del cafe
				pregunta = 1 #la variable cambia de 0 a 1, indicando que el robot cambio de opcion en el color
				avanzar()# sigue avanzando


			#NEGRO 
			if colorDetectado.value() < 8: # despues de que detectó el cafe, avanza y pregunta si es negro, esto es por si había confundido el cafe con el negro
			#en caso de que realmente sea egro y no cafe, estonces hará las funciones que tiene que hacer con el color negro.
				parar() #para
				pregunta = 1 #la variable pregunta cambia de 0 a 1, esto quiere decir que el robort se había equivocado en la primera decisión
				print("Negro", colorDetectado.value()) #imprime en pantalla que detectó el negro e imprime el valor detectado por sensor
				ev3.Sound.tone(500,1000).wait() #Reproduce un Beep largo el robot, esto hace referencia a que localizó una basura.
				moverGarra() #Mueve la garra, simbolo de que recoge la basura.
				contador += 1 #el contador se incrementa en 1, irá acumulando el numero de basuras.
				gira_derecha() #girará a la derecha para despues seguir avanzando.
				sleep(3) #todo lo anterior lo hará en 3 segundos, sin contar lo que tarda para abrir y cerrar la garra.
			

			#AZUL
			# despues de que detectó el cafe, avanza y pregunta si es azul, esto es por si había confundido el cafe con el azul
			if colorDetectado.value() >= 12 and colorDetectado.value() <=20:# si el nuevo valor detectado es de 12 a 20, entonce...
				pregunta = 1 # la variable pregunta cambia de 0 a 1, esto quiere decir que el robort se había equivocado en la primera decisión
				gira_izquierda() #girará a la izquierda, esto porque no puede meterse o quedarse en el azul que representa el agua.
				print("****************AZUL: ",colorDetectado.value()) #imprime en pantalla que es azul y el valor del color detectado.
				

			#COLOR CORRECTO (rOJO)
			if pregunta == 0: # Si la variable es igual a 0, quiere decir que el color detectado es correcto y hará las instrucciones para el colr rojo
				parar() #Se detiene
				
				while contador > 0: #este bucle es para depositar el numero de basuras recolectadas, 
				#mientras el contador de basuras sea mayor a 0, hará las intrucciones que hay dentro
					ev3.Sound.tone(100,400).wait() #Reproduce un beep corto por cada basura que deja.
					
					motor_a.run_direct(duty_cycle_sp=-60)#cierra garra
					sleep(0.5)
					motor_a.stop(stop_action='brake')#para el motor
					
					motor_a.run_direct(duty_cycle_sp=60)#abre garra
					sleep(0.28)
					motor_a.stop(stop_action='brake')#para el motor
					sleep(0.3)
					contador -= 1 #Decrementa el contador en 1, osea que le reta una basura al número total de basuras que tiene.

				contador = 0 #Despues de salir del while, la variable contador la vuelve a dejar en 0 para evitar que se quede con restos la variable
				gira_izquierda() #gira a la izquierda para seguir.
			
			pregunta = 0 #Si el color detectado fuera otro, la variable pregunta llegaria aqui con 1 y como termina aqui la funcion, la vuelve a dejar en 0





	#en caso de que sea oprimido el boton rojo (sensor touch), el programa sale del while.
	motor_left.stop() #Detiene el motor izquierdo.
	motor_right.stop()#Detiene el motor derecho.
	motor_a.stop(stop_action='brake')#Detiene el Medium_Motor
	print("Programa finalizado") #Imprime un mensaje de Programa finalizado 
	exit() #Sale del programa.

garry()#Aqui es donde se mada a llamar a la funcion garry que es la que tiene todo lo que el robot debe hacer.