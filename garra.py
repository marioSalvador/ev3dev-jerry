#!/usr/bin/env python3 
import ev3dev.ev3 as ev3
from time import sleep
import threading
motor_left = ev3.LargeMotor('outB')
motor_right = ev3.LargeMotor('outC')
motor_a = ev3.MediumMotor('outA')
infrarojo = ev3.InfraredSensor()
f_infraRed = 0


def moverGarra():
	touch_sensor = ev3.TouchSensor()
	assert touch_sensor

	motor_a.run_direct(duty_cycle_sp=-60)
	sleep(0.6)
	motor_a.stop(stop_action='brake')
	sleep(0.6)
	motor_a.run_direct(duty_cycle_sp=60)
	sleep(0.58)
	motor_a.stop(stop_action='brake')

moverGarra()