#!/usr/bin/env python3 
import ev3dev.ev3 as ev3
from time import sleep
import threading
motor_left = ev3.LargeMotor('outB')
motor_right = ev3.LargeMotor('outC')
motor_a = ev3.MediumMotor('outA')
infrarojo = ev3.InfraredSensor()
f_infraRed = 0


#la velocidad debe estar en 60 y el sleep en 0.6'para que abra de forma correcta
def moverGarra():
	touch_sensor = ev3.TouchSensor()
	assert touch_sensor
	motor_a.run_direct(duty_cycle_sp=-60)
	sleep(0.6)
	motor_a.stop(stop_action='brake')
	sleep(0.6)
	motor_a.run_direct(duty_cycle_sp=60)
	sleep(0.58)
	motor_a.stop(stop_action='brake')



def avanzar(): 
	motor_right.run_forever(speed_sp=1000)
	motor_left.run_forever(speed_sp=1000)

def avanzaObjeto(): 
	motor_right.run_forever(speed_sp=1000)
	motor_left.run_forever(speed_sp=1000)
	sleep(1.5)

def parar():
	motor_right.stop(stop_action='brake')
	motor_left.stop(stop_action='brake')


def gira_izquierda():
	motor_left.run_direct(duty_cycle_sp=-100)
	motor_right.run_direct(duty_cycle_sp=100)
	sleep(1)
	parar()

def gira_derecha():
	motor_left.run_direct(duty_cycle_sp=100)
	motor_right.run_direct(duty_cycle_sp=-100)
	sleep(1)
	parar()



def identificaColor():

	color_sensor = ev3.ColorSensor()
	assert color_sensor
	touch_sensor = ev3.TouchSensor()
	assert touch_sensor
	infrarojo = ev3.InfraredSensor()

	
	while not touch_sensor.is_pressed:
		colorDetectado = color_sensor.color

		avanzar()
		print("-------CAFE")


		if infrarojo.value() < 35:																					
			parar()
			ev3.Sound.speak()
			print("OBJETO DETECTADO")
			gira_derecha()

		elif colorDetectado == ev3.ColorSensor.COLOR_BLACK:
			parar()
			print("////////NEGRO")
			#moverGarra()

		elif colorDetectado == ev3.ColorSensor.COLOR_BLUE:
			gira_izquierda()
			print("AZUL")

		elif colorDetectado == ev3.ColorSensor.COLOR_RED:
			parar()
			print("ROJO")
			
	motor_left.stop()
	motor_right.stop()
	motor_a.stop(stop_action='brake')
	print("Se paró hilo_1")
	exit()


#CAlibra
#CAFE   32
#ROJO   45 
#AZUL   14
#NEGRO  7

def identificaColorRGB():
	colorDetectado = ev3.ColorSensor()
	touch_sensor = ev3.TouchSensor()
	assert touch_sensor
	infrarojo = ev3.InfraredSensor()


	while not touch_sensor.is_pressed:
		colorDetectado.mode = 'RGB-RAW'
		print(colorDetectado.value())

	motor_left.stop()
	motor_right.stop()
	motor_a.stop(stop_action='brake')
	print("Se paró hilo_1")
	exit()

def identificaColorREF():
	nBasuras = 0
	colorDetectado = ev3.ColorSensor()
	touch_sensor = ev3.TouchSensor()
	assert touch_sensor
	infrarojo = ev3.InfraredSensor()


	while not touch_sensor.is_pressed:
		colorDetectado.mode = 'REF-RAW'
		
		if infrarojo.value() < 35:																					
			parar()
			print("###OBJETO DETECTADO####")
			gira_derecha()
	
		#ROJO	
		elif colorDetectado.value() < 595:
			parar()
			print("++++++++++++ROJO: ",colorDetectado.value())
			print("el numero de basuras son: ",nBasuras)
		
		#CAFE
		elif colorDetectado.value() >= 610 and colorDetectado.value() <= 635:
			avanzar()
			print("------------CAFE: ",colorDetectado.value())

		#AZUL
		elif colorDetectado.value() >= 640 and colorDetectado.value() <=641:
			gira_izquierda()
			print("****************AZUL: ",colorDetectado.value())
		elif colorDetectado.value() >= 642 and colorDetectado.value() <=643:
			gira_derecha()
			print("****************AZUL: ",colorDetectado.value())

		#NEGRO
		elif colorDetectado.value() >= 644:
			print("////////////NEGRO: ",colorDetectado.value())
			nBasuras += 1
			moverGarra()
			sleep(1.78)
	

def identificaColorREFLECT():
	nBasuras = 0
	colorDetectado = ev3.ColorSensor()
	touch_sensor = ev3.TouchSensor()
	assert touch_sensor
	infrarojo = ev3.InfraredSensor()


	while not touch_sensor.is_pressed:
		colorDetectado.mode = 'REF-RAW'
		
		if infrarojo.value() < 35:																					
			parar()
			print("###OBJETO DETECTADO####")
			gira_derecha()
	
		#ROJO	
		elif colorDetectado.value() < 595:
			parar()
			print("++++++++++++ROJO: ",colorDetectado.value())
			print("el numero de basuras son: ",nBasuras)
		
		#CAFE
		elif colorDetectado.value() >= 610 and colorDetectado.value() <= 635:
			avanzar()
			print("------------CAFE: ",colorDetectado.value())

		#AZUL
		elif colorDetectado.value() >= 640 and colorDetectado.value() <=641:
			gira_izquierda()
			print("****************AZUL: ",colorDetectado.value())
		elif colorDetectado.value() >= 642 and colorDetectado.value() <=643:
			gira_derecha()
			print("****************AZUL: ",colorDetectado.value())

		#NEGRO
		elif colorDetectado.value() >= 644:
			print("////////////NEGRO: ",colorDetectado.value())
			nBasuras += 1
			moverGarra()
			sleep(1.78)

#t1 = threading.Thread(name="hilo_1", target=identificaColor)
#t2 = threading.Thread(name="hilo_2", target=paroEmergencia)
#t1.start()
#t2.start()

#identificaColor()
identificaColorRGB()
#identificaColorREF()
#identificaColorREFLECT()