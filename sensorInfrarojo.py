#!/usr/bin/env python3
from time import sleep																																																												
import ev3dev.ev3 as ev3					

leftMotor = ev3.LargeMotor('outB')						
rightMotor = ev3.LargeMotor('outC')					

leftMotor.run_forever(speed_sp = 400)						
rightMotor.run_forever(speed_sp = 400)

touch_sensor = ev3.TouchSensor()
assert touch_sensor

infrarojo = ev3.InfraredSensor()
assert infrarojo

#funcion para avanzar hacia adelante
def avanzar(): 
	motor_right.run_forever(speed_sp = 1000)
	motor_left.run_forever(speed_sp = 1000)

def parar():
	motor_right.stop(stop_action="brake")
	motor_left.stop(stop_action="brake")
while not touch_sensor.is_pressed:
#--------------------------------------------------------------
#Funciones para esquivar el obstáculo yendo por la izquierda o la derecha cuando el sensor infrarojo detecta un obstáculo y retoma
#su camino después de esquivar el objeto
	bandera = 1 
	while bandera == 1:							#Bandera 
		avanzar()											#Función que indica al robot que empiece a recorrer su camino
		if infrarojo.value() < 45:		#Condición para saber si detecta algún objeto a distancia
	#		esquivaDerecha()						#Si lo detectó manda a llamar a la función que esquiva primero hacia la derecha si no encuentra agua 
	#		irDerecho()									#Función para retomar el camino ya que esquivó el obstáculo

	#	elif infrarojo.value() < 43:	#Condición auxiliar si no se cumplió la anterior o el robot detectó agua o algo que no le permitió girar a la derecha
	#		esquivaIzquierda()					#Si detecta un objeto intenta girar a la izquierda 
	#		irDerecho()									#Función para retomar el camino ya que esquivó el obstáculo

		else:													#Freno de emergencia por si detecta algún objeto y no cumple ninguna de las condiciones anteriores
			parar()
